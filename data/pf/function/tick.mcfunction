execute as @a at @s unless score @s pf_newPlayer matches 1 run function pf:welcome
execute as @a at @s if score @s pf_gamemode matches 0..3 run function pf:triggers/gamemode

execute as @a if score @s pf_health matches 0 run effect clear @s minecraft:health_boost
execute as @a at @s if score @s pf_health matches 1..10 run function pf:triggers/health

scoreboard players enable @a pf_gamemode
scoreboard players enable @a pf_health
scoreboard players set @a pf_gamemode -1
scoreboard players set @a pf_health -1
