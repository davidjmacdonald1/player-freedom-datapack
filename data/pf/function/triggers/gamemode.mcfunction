execute if score @s pf_gamemode matches 0 run function pf:triggers/survival
execute if score @s pf_gamemode matches 1 run function pf:triggers/creative
execute if score @s pf_gamemode matches 2 run function pf:triggers/adventure
execute if score @s pf_gamemode matches 3 run function pf:triggers/spectator

playsound minecraft:block.amethyst_block.resonate player @s
