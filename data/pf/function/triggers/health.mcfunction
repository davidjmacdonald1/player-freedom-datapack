effect clear @s minecraft:health_boost

scoreboard players remove @s pf_health 1
execute store result storage minecraft:macro input.level int 1 run scoreboard players get @s pf_health
function pf:triggers/set_health with storage minecraft:macro input

effect give @s minecraft:instant_health 1 5 true
playsound minecraft:entity.warden.heartbeat player @s
