scoreboard objectives remove pf_newPlayer
scoreboard objectives remove pf_gamemode
scoreboard objectives remove pf_health

team remove survival
team remove creative
team remove adventure
team remove spectator

datapack disable "file/player-freedom-datapack"
datapack disable "file/player-freedom-datapack.zip"
