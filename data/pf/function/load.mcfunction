scoreboard objectives add pf_newPlayer dummy
scoreboard objectives add pf_gamemode trigger
scoreboard objectives add pf_health trigger

team add survival "Survival"
team add creative "Creative"
team add adventure "Adventure"
team add spectator "Spectator"

team modify survival color white
team modify creative color yellow
team modify adventure color aqua
team modify spectator color gray
